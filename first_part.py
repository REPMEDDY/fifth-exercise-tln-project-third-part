import lxml.etree as xml_parser
import os
import time


class first_part:
    """
    Class of the first module
    """

    def link_slot_with_gramrel(self):
        """
        Link each slot of file semagrams.xml to the corresponding grammar relation
        :return: None
        """
        total_links_slot_with_semagram = 0
        matches = 0
        final_matches = 0
        slot_name = ""
        gramrel_name = ""
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        semagram_xml_names = os.listdir("./semagram_xml")
        for semagram_xml_name in semagram_xml_names:
            if not semagram_xml_name.startswith("."):
                slot_gramrel_file = open("./slot_gramrel_file/" + semagram_xml_name.replace(".xml", "") + "_slot_gramrel_file.txt", "w")
                gramrels = xml_parser.parse("./semagram_xml/" + semagram_xml_name).getroot()
                for semagram in semagrams:
                    if semagram.attrib["name"] == semagram_xml_name.replace(".xml",""):
                        links_slot_with_gramrel = 0
                        for slot in semagram:
                            if slot.attrib["name"] != "generalization" and slot.attrib["name"] != "specialization":
                                for gramrel in gramrels:
                                    for value in slot:
                                        for coll in gramrel:
                                            if coll.text == value.text[:value.text.find('#')]:
                                                matches = matches + 1
                                    if matches > final_matches:
                                        final_matches = matches
                                        slot_name = slot.attrib["name"]
                                        gramrel_name = gramrel.attrib["name"]
                                    matches = 0
                                if final_matches > 0:
                                    total_links_slot_with_semagram += 1
                                    links_slot_with_gramrel += 1
                                    slot_gramrel_file.write("<" + slot_name + " : " + gramrel_name + ">\n")
                                    self.write_statistics("Links of slot " + slot.attrib["name"] + " of semagram " +
                                                      semagram_xml_name.replace(".xml", "") + " --> " + "1" + "\n")
                                else:
                                    self.write_statistics("Links of slot " + slot.attrib["name"] + " of semagram " +
                                                          semagram_xml_name.replace(".xml", "") + " --> " + "0" + "\n")
                                final_matches = 0
                        self.write_statistics("Links of semagram " + semagram_xml_name.replace(".xml", "") + " --> " + str(links_slot_with_gramrel) + "\n" +
                                              "\n--------------------------------------------------------------------------------\n\n")
                slot_gramrel_file.close()
        self.write_statistics("Total links --> " + str(total_links_slot_with_semagram))


    def reset_statistics_file(self):
        """
        Reset the content of the statistics file
        :return: None
        """
        statistics_file_second_part = open("./statistics_file/statistics_file_first_part.txt", "w")
        statistics_file_second_part.close()


    def write_statistics(self, statistic_text):
        """
        Write statistics on the statistic file
        :param statistic_text: the text to write on file
        :return: None
        """
        statistics_file_second_part = open("./statistics_file/statistics_file_first_part.txt", "a")
        statistics_file_second_part.write(statistic_text)
        statistics_file_second_part.close()


if __name__ == "__main__":
    class_instance = first_part()
    time_start = time.time()
    class_instance.reset_statistics_file()
    class_instance.link_slot_with_gramrel()
    class_instance.write_statistics("\nTime of execution --> " + str('%.3f' % float(time.time() - time_start)) + " seconds")