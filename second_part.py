import lxml.etree as xml_parser
import nltk
import urllib
import io
import gzip
import json
import urllib.parse
import urllib.request
from nltk.corpus import wordnet as wn
import gensim
import os
import time
import warnings

slots = []
gramrels = []
model = None
values_found = 0
values_accepted_with_gensim = 0
values_accepted_by_user = 0
total_true_positives = 0
total_false_positives = 0
total_false_negatives = 0
pos_tags_already_found = None
babelnet_ids_already_found = None
wordnet_ids_already_found = None


class second_part:
    """
    Class of the second module
    """

    def get_babelnet_parameters_to_disambiguate_word_in_sentence(self, sentence):
        """
        Set and return parameters of BabelNet to disambiguate a word in a sentence
        :param sentence: the sentence to send to BabelNet
        :return: BabelNet parameters
        """
        babelnet_parameters = {
            'text': sentence,
            'lang': 'EN',
            'key': '8d07527c-791d-4927-b729-15e5185cd1b2'
        }
        return babelnet_parameters


    def get_babelnet_parameters_to_get_wordnet_offset(self, babelnet_id):
        """
        Set and return parameters of BabelNet to get the WordNet offset of a BabelNet ID
        :param babelnet_id: BabelNet ID to process
        :return: BabelNet parameters
        """
        babelnet_parameters = {
            'id': babelnet_id,
            'key': '8d07527c-791d-4927-b729-15e5185cd1b2'
        }
        return babelnet_parameters


    def get_babelnet_id(self, sentence, value):
        """
        Get the BabelNet ID of a word in a sentence
        :param sentence: the sentence
        :param value: the word to disambiguate
        :return: the BabelNet ID
        """
        url = 'https://babelfy.io/v1/disambiguate?' + urllib.parse.urlencode(self.get_babelnet_parameters_to_disambiguate_word_in_sentence(sentence))
        request = urllib.request.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib.request.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            buf = io.BytesIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read().decode('utf-8'))
            for result in data:
                charFragment = result.get('charFragment')
                cfStart = charFragment.get('start')
                cfEnd = charFragment.get('end')
                if sentence[cfStart : cfEnd + 1] == value:
                    return result['babelSynsetID']
        return ""


    def get_wordnet_id(self, babelnet_id):
        """
        Get the WordNet ID of a BabelNet ID
        :param babelnet_id: the BabelNet ID to process
        :return: the WordNet ID
        """
        wordnet_id = ""
        url = 'https://babelnet.io/v4/getSynset?' + urllib.parse.urlencode(self.get_babelnet_parameters_to_get_wordnet_offset(babelnet_id))
        request = urllib.request.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib.request.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            buf = io.BytesIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read().decode('utf-8'))
            wordnet_offsets = data['wnOffsets']
            if len(wordnet_offsets) == 1:
                wordnet_offset = wordnet_offsets[0]['id']
                offset_in_number_form = wordnet_offset[3:len(wordnet_offset)-1]
                pos_tag = wordnet_offset[len(wordnet_offset)-1:len(wordnet_offset)]
                # wordnet_id = str(wn._synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2]
                wordnet_id = str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2]
            elif len(wordnet_offsets) > 1:
                for wordnet_offset_struct in wordnet_offsets:
                    wordnet_offset = wordnet_offset_struct['id']
                    offset_in_number_form = wordnet_offset[3:len(wordnet_offset)-1]
                    pos_tag = wordnet_offset[len(wordnet_offset) - 1:len(wordnet_offset)]
                    if str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2] not in wordnet_id:
                        wordnet_id += str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2] + ","
                wordnet_id = wordnet_id[:len(wordnet_id) - 1]
        return wordnet_id


    def get_pos_tag(self, sentence, value):
        """
        Get the POS tag of a word in a sentence
        :param sentence: the sentence
        :param value: the word to process
        :return: the POS tag
        """
        pos_tags = nltk.pos_tag(nltk.word_tokenize(sentence))
        for pos_tag in pos_tags:
            if pos_tag[0] == value:
                return pos_tag[1]
        return ""


    def reset_slots_and_gramrels(self):
        """
        Reset lists of slots and grammar relations
        :return: None
        """
        global slots
        global gramrels
        slots = []
        gramrels = []


    def reset_statistics_file(self):
        """
        Reset the statistics file
        :return: None
        """
        statistics_file_second_part = open("./statistics_file/statistics_file_second_part.txt", "w")
        statistics_file_second_part.close()


    def init_semagrams_after_first_and_second_part(self):
        """
        initialize the content of the semagrams file of first and second part
        :return: None
        """
        semagrams_after_first_and_second_part = open("./semagrams_xml/semagrams_after_first_and_second_part.xml", "w")
        semagrams_after_first_and_second_part.write("<semagrams>")
        semagrams_after_first_and_second_part.close()


    def finish_semagrams_after_first_and_second_part(self):
        """
        Finish the content of the semagrams file of first and second part
        :return: None
        """
        semagrams_after_first_and_second_part = open("./semagrams_xml/semagrams_after_first_and_second_part.xml", "a")
        semagrams_after_first_and_second_part.write("\n</semagrams>")
        semagrams_after_first_and_second_part.close()


    def filter_wikipedia_sentences(self):
        """
        Filter wikipedia sentences
        :return: None
        """
        print("Starting collecting sentences\n")
        wikipedia_senteces_filtered_file = open("./wikipedia_sentences/wikisent2_filtered_for_second_part.txt", "w")
        semagrams = []
        semagram_xml_names = sorted(os.listdir("./semagram_xml"))
        for semagram_xml_name in semagram_xml_names:
            if not semagram_xml_name.startswith("."):
                semagrams.append(semagram_xml_name.replace(".xml", ""))
        for semagram in semagrams:
            print("Collecting sentences for " + semagram + " ...")
            wikipedia_senteces_file = open("./wikipedia_sentences/wikisent2.txt", "r")
            for line in wikipedia_senteces_file:
                if semagram in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" "):
                    wikipedia_senteces_filtered_file.write(line)
            wikipedia_senteces_file.close()
        wikipedia_senteces_filtered_file.close()
        print("Collecting sentences successfully completed\n")


    def get_slot_to_change(self, semagram_xml_name):
        """
        Store slots of a semagram where will be added new values
        :param semagram_xml_name: the semagram name
        :return: None
        """
        global slots
        global gramrels
        if os.stat("./slot_gramrel_file/" + semagram_xml_name + "_slot_gramrel_file.txt").st_size > 0:
            slot_gramrel_file = open("./slot_gramrel_file/" + semagram_xml_name + "_slot_gramrel_file.txt", "r")
            for line in slot_gramrel_file:
                slots.append(line[1:line.find(":") - 1])
                gramrels.append(line[line.find(":") + 2:line.find(">")])
            slot_gramrel_file.close()
        else:
            print("\nSemagram " + semagram_xml_name + " hasn't any link between slots and gramrels\n")
            self.write_statistics("Semagram " + semagram_xml_name + " hasn't any link between slots and gramrels\n")


    def get_slot_index(self, slot_name):
        """
        Get the index of a slot in slots list
        :param slot_name: the slot name
        :return: the slot index if exist or -1
        """
        try:
            slot_index = slots.index(slot_name)
            return slot_index
        except:
            return -1


    def print_definition_of_wordnet_id(self, wordnet_id, coll, coll_index):
        """
        Print the definition for each WordNet ID
        :param wordnet_id: the WordNet ID
        :param coll: name of the value
        :param coll_index: index of coll
        :return: None
        """
        wordnet_ids = wordnet_id.split(",")
        if coll_index == 0:
            print("\n", end="")
        for wordnet_id in wordnet_ids:
            print("Added value " + coll + " with definition: " + wn.synset(wordnet_id).definition())
        print("\n", end="")


    def print_error(self, error_text, coll_index):
        """
        Print the error occurred while processing colls
        :param error_text: the text of the error
        :param coll_index: index of coll
        :return: None
        """
        if coll_index == 0:
            print("\n", end="")
        print(error_text + "\n")


    def get_coll_to_add(self, index_of_slot, semagram_xml_name):
        """
        Get new values to add and filter them
        :param index_of_slot: the index of slot
        :param semagram_xml_name: semagram name
        :return: new values to add
        """
        global values_found, values_accepted_with_gensim, values_accepted_by_user, total_true_positives, total_false_positives, total_false_negatives
        colls = []
        gramrel_name = gramrels[index_of_slot]
        gramrels_parser = xml_parser.parse("./semagram_xml/" + semagram_xml_name + ".xml").getroot()
        for gramrel in gramrels_parser:
            if gramrel_name == gramrel.attrib["name"]:
                for coll in gramrel:
                    colls.append(coll.text)
        false_negatives = self.found_false_negatives(colls, slots[index_of_slot], semagram_xml_name)
        colls, true_positives = self.remove_duplicate_value(colls, slots[index_of_slot], semagram_xml_name)
        self.write_statistics("Values of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " found --> " + str(len(colls)))
        values_found += len(colls)
        colls = self.remove_unsatisfactory_value_with_gensim(colls, slots[index_of_slot], semagram_xml_name)
        self.write_statistics("\nValues of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " accepted with gensim --> " + str(len(colls)))
        values_accepted_with_gensim += len(colls)
        colls, false_positives = self.ask_the_user(colls, slots[index_of_slot], semagram_xml_name)
        self.write_statistics("\nValues of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " accepted by user --> " + str(len(colls)))
        values_accepted_by_user += len(colls)
        self.write_statistics("\nTrue positives of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " found --> " + str(true_positives))
        total_true_positives += true_positives
        self.write_statistics("\nFalse positives of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " found --> " + str(false_positives))
        total_false_positives += false_positives
        self.write_statistics("\nFalse negatives of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " found --> " + str(false_negatives))
        total_false_negatives += false_negatives
        self.write_statistics("\nPrecision of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " --> " + str('%.3f' % float(true_positives / (true_positives + false_positives))))
        self.write_statistics("\nRecall of semagram " + semagram_xml_name + " in slot " + slots[index_of_slot] + " --> " + str('%.3f' % float(true_positives / (true_positives + false_negatives))) + "\n")
        return colls


    def ask_the_user(self, colls, slot_name, semagram_name):
        """
        Ask the user if he wants add the proposed new value
        :param colls: values
        :param slot_name: slot name
        :param semagram_name: semagram name
        :return: values that the user has accepted
        """
        colls_to_maintain = []
        false_positives = 0
        for coll in colls:
            choice = input(semagram_name + " --> " + slot_name + " --> " + coll + ": ")
            if "y" in choice:
                colls_to_maintain.append(coll)
            else:
                false_positives += 1
        return colls_to_maintain, false_positives


    def remove_unsatisfactory_value_with_gensim(self, colls, slot_name, semagram_xml_name):
        """
        Filter values with Gensim
        :param colls: values
        :param slot_name: slot name
        :param semagram_xml_name: semagram name
        :return: values accepted by Gensim
        """
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        colls_to_maintain = []
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_xml_name:
                for slot in semagram:
                    if slot.attrib["name"] == slot_name:
                        for coll in colls:
                            for value in slot:
                                try:
                                    if float(model.similarity(coll, value.text[:value.text.find("#")])) >= 0.3 and coll not in colls_to_maintain:
                                        colls_to_maintain.append(coll)
                                except:
                                    pass
        return colls_to_maintain


    def add_categorize_babelnet_id_and_wordnet_id_to_coll(self, coll, semagram_name):
        """
        Add the POS tag, the BabelNet ID and the WordNet ID to a value
        :param coll: the value
        :param semagram_name: semagram name
        :return: the POS tag, the BabelNet ID and the WordNet ID
        """
        global pos_tags_already_found, babelnet_ids_already_found, wordnet_ids_already_found
        if coll in pos_tags_already_found and coll in babelnet_ids_already_found and coll in wordnet_ids_already_found:
            return pos_tags_already_found.get(coll), babelnet_ids_already_found.get(coll), wordnet_ids_already_found.get(coll)
        else:
            pos_tag_to_return = ""
            babelnet_id_to_return = ""
            wordnet_id_to_return = ""
            wikipedia_sentences_file = open("./wikipedia_sentences/wikisent2_filtered_for_second_part.txt", "r")
            pos_tags = []
            babelnet_ids = []
            wordnet_ids = []
            matches = 0
            for line in wikipedia_sentences_file:
                if matches < 5:
                    if coll in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" ") \
                            and semagram_name in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" "):
                        wordnet_id = ""
                        matches += 1
                        pos_tag = self.get_pos_tag(line, coll)
                        babelnet_id = self.get_babelnet_id(line, coll)
                        if babelnet_id != "":
                            wordnet_id = self.get_wordnet_id(babelnet_id)
                        if len(pos_tag) > 0:
                            pos_tag = pos_tag[0]
                            if pos_tag not in pos_tags:
                                pos_tags.append(pos_tag)
                        if len(babelnet_id) > 0:
                            if babelnet_id not in babelnet_ids:
                                babelnet_ids.append(babelnet_id)
                        if len(wordnet_id) > 0:
                            if wordnet_id not in wordnet_ids:
                                wordnet_ids.append(wordnet_id)
                else:
                    break
            pos_tag_to_return += "#"
            for pos_tag in pos_tags:
                pos_tag_to_return += pos_tag + "|"
            pos_tag_to_return = pos_tag_to_return[:len(pos_tag_to_return) - 1]
            pos_tags_already_found[coll] = pos_tag_to_return
            for babelnet_id in babelnet_ids:
                babelnet_id_to_return += babelnet_id + ","
            babelnet_id_to_return = babelnet_id_to_return[:len(babelnet_id_to_return) - 1]
            babelnet_ids_already_found[coll] = babelnet_id_to_return
            for wordnet_id in wordnet_ids:
                wordnet_id_to_return += wordnet_id + ","
            wordnet_id_to_return = wordnet_id_to_return[:len(wordnet_id_to_return) - 1]
            wordnet_ids_already_found[coll] = wordnet_id_to_return
            wikipedia_sentences_file.close()
            return pos_tag_to_return, babelnet_id_to_return, wordnet_id_to_return


    def remove_duplicate_value(self, colls, slot_name, semagram_xml_name):
        """
        Remove already existing values in semagrams file
        :param colls: values
        :param slot_name: slot name
        :param semagram_xml_name: semagram name
        :return: values filtered
        """
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        colls_to_remove = []
        true_positives = 0
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_xml_name:
                for slot in semagram:
                    if slot.attrib["name"] == slot_name:
                        for coll in colls:
                            for value in slot:
                                if coll == value.text[:value.text.find("#")]:
                                    colls_to_remove.append(coll)
        for coll_to_remove in colls_to_remove:
            colls.remove(coll_to_remove)
            true_positives += 1
        return colls, true_positives


    def found_false_negatives(self, colls, slot_name, semagram_xml_name):
        """
        Found the number of false negatives of a slot
        :param colls: value already in initial semagrams file
        :param slot_name: name of slot
        :return: number of false negatives
        """
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        false_negatives = 0
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_xml_name:
                for slot in semagram:
                    if slot.attrib["name"] == slot_name:
                        for value in slot:
                            if value.text[:value.text.find("#")] not in colls:
                                false_negatives += 1
        return false_negatives


    def add_values(self, semagram_xml_name):
        """
        Write the semagrams file of first and second part adding new values
        :param semagram_xml_name: semagram name
        :return: None
        """
        global pos_tags_already_found, babelnet_ids_already_found, wordnet_ids_already_found
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        semagrams_after_first_and_second_part = open("./semagrams_xml/semagrams_after_first_and_second_part.xml", "a")
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_xml_name:
                pos_tags_already_found = {}
                babelnet_ids_already_found = {}
                wordnet_ids_already_found = {}
                semagrams_after_first_and_second_part.write("\n\t<semagram name=\"" + semagram.attrib["name"] + "\" synset=\"" + semagram.attrib["synset"] +
                                              "\" babelsynset=\"" + semagram.attrib["babelsynset"] + "\">")
                for slot in semagram:
                    semagrams_after_first_and_second_part.write("\n\t\t<slot name=\"" + slot.attrib["name"] + "\">")
                    index_of_slot = self.get_slot_index(slot.attrib["name"])
                    if index_of_slot != -1:
                        colls = self.get_coll_to_add(index_of_slot, semagram_xml_name)
                        for coll in colls:
                            pos_tag, babelnet_id, wordnet_id = self.add_categorize_babelnet_id_and_wordnet_id_to_coll(coll, semagram_xml_name)
                            if wordnet_id != "":
                                self.print_definition_of_wordnet_id(wordnet_id, coll, colls.index(coll))
                            elif wordnet_id == "" and pos_tag != "":
                                self.print_error("Added value " + coll + " but not found in BabelNet and WordNet", colls.index(coll))
                            elif wordnet_id == "" and pos_tag == "":
                                self.print_error("Value " + coll + " not found in the corpus and/or in resources", colls.index(coll))
                            semagrams_after_first_and_second_part.write("\n\t\t\t<value auto=\"yes\" babelsynset=\"" + babelnet_id +
                                                                        "\" wnSynset=\"" + wordnet_id + "\">" + coll + pos_tag + "</value>")
                    for value in slot:
                        semagrams_after_first_and_second_part.write("\n\t\t\t<value babelsynset=\"" + value.attrib["babelsynset"] + "\"" +
                                                     " wnSynset=\"" + value.attrib["wnSynset"] + "\">" + value.text + "</value>")
                    semagrams_after_first_and_second_part.write("\n\t\t</slot>")
                semagrams_after_first_and_second_part.write("\n\t</semagram>")
        semagrams_after_first_and_second_part.close()


    def write_statistics(self, statistic_text):
        """
        Write statistics on the statistic file
        :param statistic_text: the text to write on file
        :return: None
        """
        statistics_file_second_part = open("./statistics_file/statistics_file_second_part.txt", "a")
        statistics_file_second_part.write(statistic_text)
        statistics_file_second_part.close()


    def load_gensim_model(self):
        """
        Load in RAM the Gensim model file
        :return: None
        """
        global model
        print("Loading Gensim model ...\n")
        model = gensim.models.KeyedVectors.load_word2vec_format('./gensim_model/GoogleNews-vectors-negative300.bin', binary=True)


    def semagrams_processing(self):
        """
        Processing semagrams
        :return: None
        """
        semagram_xml_names = sorted(os.listdir("./semagram_xml"))
        for semagram_xml_name in semagram_xml_names:
            if not semagram_xml_name.startswith("."):
                self.get_slot_to_change(semagram_xml_name.replace(".xml", ""))
                self.add_values(semagram_xml_name.replace(".xml", ""))
                self.reset_slots_and_gramrels()
                self.write_statistics("\n--------------------------------------------------------------------------------\n\n")


if __name__ == "__main__":
    warnings.simplefilter(action='ignore', category=FutureWarning)
    class_instance = second_part()
    class_instance.reset_statistics_file()
    time_start = time.time()
    class_instance.load_gensim_model()
    # class_instance.filter_wikipedia_sentences()     Eseguito solo la prima volta per ottenere il file permanente
    class_instance.init_semagrams_after_first_and_second_part()
    class_instance.semagrams_processing()
    class_instance.finish_semagrams_after_first_and_second_part()
    class_instance.write_statistics("Total values found --> " + str(values_found))
    class_instance.write_statistics("\nTotal values accepted with gensim --> " + str(values_accepted_with_gensim))
    class_instance.write_statistics("\nTotal values accepted by user --> " + str(values_accepted_by_user))
    class_instance.write_statistics("\nTotal true positives --> " + str(total_true_positives))
    class_instance.write_statistics("\nTotal false positives --> " + str(total_false_positives))
    class_instance.write_statistics("\nTotal false negatives --> " + str(total_false_negatives))
    class_instance.write_statistics("\nAverage number of values added for each semagram --> " + str('%.3f' % float(values_accepted_by_user / 5)))
    class_instance.write_statistics("\nTotal Precision --> " + str('%.3f' % float(total_true_positives / (total_true_positives + total_false_positives))))
    class_instance.write_statistics("\nTotal Recall --> " + str('%.3f' % float(total_true_positives / (total_true_positives + total_false_negatives))))
    class_instance.write_statistics("\nTotal Accuracy --> " + str('%.3f' % float(total_true_positives / values_accepted_with_gensim)))
    class_instance.write_statistics("\nTime of execution --> " + str(int(time.time() - time_start)) + " seconds")