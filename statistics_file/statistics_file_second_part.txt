Values of semagram bolt in slot material found --> 98
Values of semagram bolt in slot material accepted with gensim --> 15
Values of semagram bolt in slot material accepted by user --> 4
True positives of semagram bolt in slot material found --> 2
False positives of semagram bolt in slot material found --> 11
False negatives of semagram bolt in slot material found --> 1
Precision of semagram bolt in slot material --> 0.154
Recall of semagram bolt in slot material --> 0.667

--------------------------------------------------------------------------------

Values of semagram dirigible in slot user found --> 12
Values of semagram dirigible in slot user accepted with gensim --> 1
Values of semagram dirigible in slot user accepted by user --> 0
True positives of semagram dirigible in slot user found --> 1
False positives of semagram dirigible in slot user found --> 1
False negatives of semagram dirigible in slot user found --> 4
Precision of semagram dirigible in slot user --> 0.500
Recall of semagram dirigible in slot user --> 0.200

--------------------------------------------------------------------------------

Values of semagram hook in slot part found --> 99
Values of semagram hook in slot part accepted with gensim --> 17
Values of semagram hook in slot part accepted by user --> 0
True positives of semagram hook in slot part found --> 1
False positives of semagram hook in slot part found --> 17
False negatives of semagram hook in slot part found --> 1
Precision of semagram hook in slot part --> 0.056
Recall of semagram hook in slot part --> 0.500
Values of semagram hook in slot material found --> 97
Values of semagram hook in slot material accepted with gensim --> 12
Values of semagram hook in slot material accepted by user --> 2
True positives of semagram hook in slot material found --> 3
False positives of semagram hook in slot material found --> 10
False negatives of semagram hook in slot material found --> 0
Precision of semagram hook in slot material --> 0.231
Recall of semagram hook in slot material --> 1.000

--------------------------------------------------------------------------------

Values of semagram rod in slot colorPattern found --> 89
Values of semagram rod in slot colorPattern accepted with gensim --> 5
Values of semagram rod in slot colorPattern accepted by user --> 2
True positives of semagram rod in slot colorPattern found --> 1
False positives of semagram rod in slot colorPattern found --> 3
False negatives of semagram rod in slot colorPattern found --> 2
Precision of semagram rod in slot colorPattern --> 0.250
Recall of semagram rod in slot colorPattern --> 0.333
Values of semagram rod in slot material found --> 97
Values of semagram rod in slot material accepted with gensim --> 19
Values of semagram rod in slot material accepted by user --> 13
True positives of semagram rod in slot material found --> 3
False positives of semagram rod in slot material found --> 6
False negatives of semagram rod in slot material found --> 0
Precision of semagram rod in slot material --> 0.333
Recall of semagram rod in slot material --> 1.000

--------------------------------------------------------------------------------

Values of semagram wire in slot colorPattern found --> 97
Values of semagram wire in slot colorPattern accepted with gensim --> 14
Values of semagram wire in slot colorPattern accepted by user --> 5
True positives of semagram wire in slot colorPattern found --> 3
False positives of semagram wire in slot colorPattern found --> 9
False negatives of semagram wire in slot colorPattern found --> 1
Precision of semagram wire in slot colorPattern --> 0.250
Recall of semagram wire in slot colorPattern --> 0.750
Values of semagram wire in slot material found --> 97
Values of semagram wire in slot material accepted with gensim --> 21
Values of semagram wire in slot material accepted by user --> 13
True positives of semagram wire in slot material found --> 3
False positives of semagram wire in slot material found --> 8
False negatives of semagram wire in slot material found --> 0
Precision of semagram wire in slot material --> 0.273
Recall of semagram wire in slot material --> 1.000

--------------------------------------------------------------------------------

Total values found --> 686
Total values accepted with gensim --> 104
Total values accepted by user --> 39
Total true positives --> 17
Total false positives --> 65
Total false negatives --> 9
Average number of values added for each semagram --> 7.800
Total Precision --> 0.207
Total Recall --> 0.654
Total Accuracy --> 0.163
Time of execution --> 1087 seconds