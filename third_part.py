import lxml.etree as xml_parser
import nltk
import urllib
import io
import gzip
import json
import urllib.parse
import urllib.request
from nltk.corpus import wordnet as wn
import gensim
import os
import time
import warnings

slots = []
gramrels = []
model = None
total_semagrams_added = 0
total_values_added = 0
pos_tags_already_found = None
babelnet_ids_already_found = None
wordnet_ids_already_found = None
semagrams_already_found = []
semagrams_third_part_ids = None
start = None
end = None
total_semantic_propagation = 0

class third_part:
    """
    Class of the third module
    """

    def get_babelnet_parameters_to_disambiguate_word_in_sentence(self, sentence):
        """
        Set and return parameters of BabelNet to disambiguate a word in a sentence
        :param sentence: the sentence to send to BabelNet
        :return: BabelNet parameters
        """
        babelnet_parameters = {
            'text': sentence,
            'lang': 'EN',
            'key': '8d07527c-791d-4927-b729-15e5185cd1b2'
        }
        return babelnet_parameters


    def get_babelnet_parameters_to_get_wordnet_offset(self, babelnet_id):
        """
        Set and return parameters of BabelNet to get the WordNet offset of a BabelNet ID
        :param babelnet_id: BabelNet ID to process
        :return: BabelNet parameters
        """
        babelnet_parameters = {
            'id': babelnet_id,
            'key': '8d07527c-791d-4927-b729-15e5185cd1b2'
        }
        return babelnet_parameters


    def get_babelnet_id(self, sentence, value):
        """
        Get the BabelNet ID of a word in a sentence
        :param sentence: the sentence
        :param value: the word to disambiguate
        :return: the BabelNet ID
        """
        url = 'https://babelfy.io/v1/disambiguate?' + urllib.parse.urlencode(self.get_babelnet_parameters_to_disambiguate_word_in_sentence(sentence))
        request = urllib.request.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib.request.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            buf = io.BytesIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read().decode('utf-8'))
            for result in data:
                charFragment = result.get('charFragment')
                cfStart = charFragment.get('start')
                cfEnd = charFragment.get('end')
                if sentence[cfStart : cfEnd + 1] == value:
                    return result['babelSynsetID']
        return ""


    def get_wordnet_id(self, babelnet_id):
        """
        Get the WordNet ID of a BabelNet ID
        :param babelnet_id: the BabelNet ID to process
        :return: the WordNet ID
        """
        wordnet_id = ""
        url = 'https://babelnet.io/v4/getSynset?' + urllib.parse.urlencode(self.get_babelnet_parameters_to_get_wordnet_offset(babelnet_id))
        request = urllib.request.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib.request.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            buf = io.BytesIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read().decode('utf-8'))
            wordnet_offsets = data['wnOffsets']
            if len(wordnet_offsets) == 1:
                wordnet_offset = wordnet_offsets[0]['id']
                offset_in_number_form = wordnet_offset[3:len(wordnet_offset)-1]
                pos_tag = wordnet_offset[len(wordnet_offset)-1:len(wordnet_offset)]
                wordnet_id = str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2]
            elif len(wordnet_offsets) > 1:
                for wordnet_offset_struct in wordnet_offsets:
                    wordnet_offset = wordnet_offset_struct['id']
                    offset_in_number_form = wordnet_offset[3:len(wordnet_offset)-1]
                    pos_tag = wordnet_offset[len(wordnet_offset) - 1:len(wordnet_offset)]
                    if str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2] not in wordnet_id:
                        wordnet_id += str(wn.synset_from_pos_and_offset(pos_tag, int(offset_in_number_form)))[8:-2] + ","
                wordnet_id = wordnet_id[:len(wordnet_id) - 1]
        return wordnet_id


    def get_pos_tag(self, sentence, value):
        """
        Get the POS tag of a word in a sentence
        :param sentence: the sentence
        :param value: the word to process
        :return: the POS tag
        """
        pos_tags = nltk.pos_tag(nltk.word_tokenize(sentence))
        for pos_tag in pos_tags:
            if pos_tag[0] == value:
                return pos_tag[1]
        return ""


    def reset_slots_and_gramrels(self):
        """
        Reset lists of slots and grammar relations
        :return: None
        """
        global slots
        global gramrels
        slots = []
        gramrels = []


    def reset_statistics_file(self):
        """
        Reset the statistics file
        :return: None
        """
        statistics_file_third_part = open("./statistics_file/statistics_file_third_part_" + str(start) + "-" + str(end) + ".txt", "w")
        statistics_file_third_part.close()


    def init_semagrams_already_found(self):
        """
        Initialize the list of semagrams already founded
        :return: None
        """
        global semagrams_already_found
        semagrams_after_first_and_second_part = xml_parser.parse("./semagrams_xml/semagrams_after_first_and_second_part.xml").getroot()
        for semagram in semagrams_after_first_and_second_part:
            semagrams_already_found.append(semagram.attrib["name"])


    def init_semagrams_after_third_part(self):
        """
        initialize the content of the semagrams file of third part
        :return: None
        """
        semagrams_after_third_part = open("./semagrams_xml/semagrams_after_third_part_" + str(start) + "-" + str(end) + ".xml", "w")
        semagrams_after_third_part.write("<semagrams>")
        semagrams_after_third_part.close()


    def finish_semagrams_after_third_part(self):
        """
        Finish the content of the semagrams file of third part
        :return: None
        """
        semagrams_after_first_and_second_part = open("./semagrams_xml/semagrams_after_first_and_second_part.xml", "r")
        semagrams_after_third_part = open("./semagrams_xml/semagrams_after_third_part_" + str(start) + "-" + str(end) + ".xml", "a")
        semagrams_after_third_part.write(semagrams_after_first_and_second_part.read().replace("<semagrams>", "").replace("</semagrams>", ""))
        semagrams_after_third_part.write("</semagrams>")
        semagrams_after_third_part.close()
        semagrams_after_first_and_second_part.close()


    def save_semagrams_after_third_part_for_semantic_propagation(self):
        """
        Save a copy of semagrams of third part for semantic propagation
        :return: None
        """
        semagrams_after_third_part_for_semantic_propagation = open("semagrams_xml/semagrams_after_third_part_for_semantic_propagation.xml", "w")
        semagrams_after_third_part = open("./semagrams_xml/semagrams_after_third_part_" + str(start) + "-" + str(end) + ".xml", "r")
        semagrams_after_third_part_for_semantic_propagation.write(semagrams_after_third_part.read())
        semagrams_after_third_part_for_semantic_propagation.write("\n</semagrams>")
        semagrams_after_third_part_for_semantic_propagation.close()
        semagrams_after_third_part.close()


    def get_slot_and_gramrel(self, semagram_xml_name):
        """
        Store slots and grammar relations from file
        :param semagram_xml_name: semagram name
        :return: None
        """
        global slots
        global gramrels
        if os.stat("./slot_gramrel_file/" + semagram_xml_name + "_slot_gramrel_file.txt").st_size > 0:
            slot_gramrel_file = open("./slot_gramrel_file/" + semagram_xml_name + "_slot_gramrel_file.txt", "r")
            for line in slot_gramrel_file:
                slots.append(line[1:line.find(":") - 1])
                gramrels.append(line[line.find(":") + 2:line.find(">")])
            slot_gramrel_file.close()
        else:
            print("\nSemagram " + semagram_xml_name + " hasn't any link between slots and gramrels\n")


    def filter_wikipedia_sentences(self):
        """
        Filter wikipedia sentences
        :return: None
        """
        print("Starting collecting sentences\n")
        wikipedia_senteces_filtered_file = open("./wikipedia_sentences/wikisent2_filtered_for_third_part.txt", "w")
        semagrams = []
        semagram_xml_names = sorted(os.listdir("./semagram_xml"))
        for semagram_xml_name in semagram_xml_names:
            if not semagram_xml_name.startswith("."):
                semagrams_similar = xml_parser.parse("./semagram_similar_xml/" + semagram_xml_name.replace(".xml", "") + "_similar.xml").getroot()
                for keyword in semagrams_similar:
                    for item in keyword:
                        semagrams.append(item.text)
        for semagram in semagrams:
            print("Collecting sentences for " + semagram + " ...")
            wikipedia_senteces_file = open("./wikipedia_sentences/wikisent2.txt", "r")
            for line in wikipedia_senteces_file:
                if semagram in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" "):
                    wikipedia_senteces_filtered_file.write(line)
            wikipedia_senteces_file.close()
        wikipedia_senteces_filtered_file.close()
        print("Collecting sentences completed successfully\n")


    def get_indexes_gramrel(self, grmarel_name):
        """
        Get the indexes of a grammar relation with the name
        :param grmarel_name: grammar relation name
        :return: the grammar relation indexes
        """
        indexes_gramrel = (index for index, element in enumerate(gramrels) if element == grmarel_name)
        return indexes_gramrel


    def get_semagrams_third_part_ids(self):
        """
        Store IDS of the new semagrams of third part
        :return: None
        """
        global semagrams_third_part_ids
        semagrams_third_part_ids = {}
        semagrams_third_part_ids_file = open("./other_files/semagrams_third_part_ids.txt", "r")
        for line in semagrams_third_part_ids_file:
            semagrams_third_part_ids[line.split(": ")[0]] = line.split(": ")[1].replace("\n", "")
        semagrams_third_part_ids_file.close()


    def remove_unsatisfactory_value_with_gensim(self, item_text, slot_name):
        """
        Filter values with Gensim
        :param item_text: value name
        :param slot_name: slot name
        :return: False if values is accepted or True if not
        """
        semagrams_after_first_and_second_part_parser = xml_parser.parse("./semagrams_xml/semagrams_after_first_and_second_part.xml").getroot()
        for semagram in semagrams_after_first_and_second_part_parser:
            for slot in semagram:
                if slot.attrib["name"] == slot_name:
                    for value in slot:
                        try:
                            if "#" in value.text:
                                final_value = value.text[:value.text.find("#")]
                            else:
                                final_value = value.text
                            if float(model.similarity(item_text, final_value)) >= 0.3:
                                return False
                        except:
                            return True
        return True


    def print_definition_of_wordnet_id(self, wordnet_id, value):
        """
        Print the definition for each WordNet ID
        :param wordnet_id: the WordNet ID
        :param coll: name of the value
        :return: None
        """
        wordnet_ids = wordnet_id.split(",")
        print("\n", end="")
        for wordnet_id in wordnet_ids:
            print("Added value " + value + " with definition: " + wn.synset(wordnet_id).definition())
        print("\n", end="")


    def add_categorize_babelnet_id_and_wordnet_id_to_value(self, value, semagram_name):
        """
        Add the POS tag, the BabelNet ID and the WordNet ID to a value
        :param value: the value
        :param semagram_name: semagram name
        :return: the POS tag, the BabelNet ID and the WordNet ID
        """
        global pos_tags_already_found, babelnet_ids_already_found, wordnet_ids_already_found
        if value in pos_tags_already_found and value in babelnet_ids_already_found and value in wordnet_ids_already_found:
            return pos_tags_already_found.get(value), babelnet_ids_already_found.get(value), wordnet_ids_already_found.get(value)
        else:
            pos_tag_to_return = ""
            babelnet_id_to_return = ""
            wordnet_id_to_return = ""
            wikipedia_sentences_file = open("./wikipedia_sentences/wikisent2_filtered_for_third_part.txt", "r")
            pos_tags = []
            babelnet_ids = []
            wordnet_ids = []
            matches = 0
            for line in wikipedia_sentences_file:
                if matches < 5:
                    if value in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" ") \
                            and semagram_name in line.replace("!", "").replace("\"", "").replace("(", "").replace(")", "").replace("?", "").replace("^", "")\
                            .replace("'", "").replace("[", "").replace("]", "").replace(",", "").replace(";", "").replace(".", "")\
                            .replace(":", "").replace("{", "").replace("}", "").split(" "):
                        wordnet_id = ""
                        matches += 1
                        pos_tag = self.get_pos_tag(line, value)
                        babelnet_id = self.get_babelnet_id(line, value)
                        if babelnet_id != "":
                            wordnet_id = self.get_wordnet_id(babelnet_id)
                        if len(pos_tag) > 0:
                            pos_tag = pos_tag[0]
                            if pos_tag not in pos_tags:
                                pos_tags.append(pos_tag)
                        if len(babelnet_id) > 0:
                            if babelnet_id not in babelnet_ids:
                                babelnet_ids.append(babelnet_id)
                        if len(wordnet_id) > 0:
                            if wordnet_id not in wordnet_ids:
                                wordnet_ids.append(wordnet_id)
                else:
                    break
            pos_tag_to_return += "#"
            for pos_tag in pos_tags:
                pos_tag_to_return += pos_tag + "|"
            pos_tag_to_return = pos_tag_to_return[:len(pos_tag_to_return) - 1]
            pos_tags_already_found[value] = pos_tag_to_return
            for babelnet_id in babelnet_ids:
                babelnet_id_to_return += babelnet_id + ","
            babelnet_id_to_return = babelnet_id_to_return[:len(babelnet_id_to_return) - 1]
            babelnet_ids_already_found[value] = babelnet_id_to_return
            for wordnet_id in wordnet_ids:
                wordnet_id_to_return += wordnet_id + ","
            wordnet_id_to_return = wordnet_id_to_return[:len(wordnet_id_to_return) - 1]
            wordnet_ids_already_found[value] = wordnet_id_to_return
            wikipedia_sentences_file.close()
            return pos_tag_to_return, babelnet_id_to_return, wordnet_id_to_return


    def ask_the_user(self, semagram_name, slot_name, value_name):
        """
        Ask the user if he wants add the proposed new value
        :param semagram_name: semagram name
        :param slot_name: slot name
        :param value_name: value name
        :return: True if the user has accepted the value or False if not
        """
        choice = input(semagram_name + " --> " + slot_name + " --> " + value_name + ": ")
        if "y" in choice:
            return True
        return False


    def get_values_to_ask_of_original_semagram(self, semagram_xml_name, semagram_to_add_name, semagrams_after_third_part):
        """
        Get values of the original semagram to ask them to user
        :param semagram_xml_name: the original semagram
        :param semagram_to_add_name: the similar semagram
        :param semagrams_after_third_part: file descriptor
        :return: None
        """
        global total_values_added
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_xml_name:
                for slot in semagram:
                    if slot.attrib["name"] not in slots:
                        semagrams_after_third_part.write("\n\t\t<slot name=\"" + slot.attrib["name"] + "\">")
                        values_added = self.ask_values_of_similiar_semagram_to_user(semagram_xml_name, semagram_to_add_name, slot.attrib["name"], 0, [], semagrams_after_third_part)
                        total_values_added += values_added
                        self.write_statistics("Values added in slot " + slot.attrib["name"] + " of semagram " + semagram_to_add_name + " --> " + str(values_added) + "\n")
                        semagrams_after_third_part.write("\n\t\t</slot>")


    def ask_values_of_similiar_semagram_to_user(self, semagram_name, semagram_to_add_name, slot_name, values_added, values_already_found, semagrams_after_third_part):
        """
        Ask the user if he wants add values of similar semagram
        :param semagram_name: the name of similar semagram
        :param semagram_to_add_name: the name of the semagram to add
        :param slot_name: the name of the slot
        :param values_added: the number of values added
        :param values_already_found: values already founded
        :param semagrams_after_third_part: file descriptor
        :return: the number of values added
        """
        semagrams = xml_parser.parse("./semagrams_xml/semagrams.xml").getroot()
        for semagram in semagrams:
            if semagram.attrib["name"] == semagram_name:
                for slot in semagram:
                    if slot.attrib["name"] == slot_name:
                        for value in slot:
                            if value.text[:value.text.find("#")] not in values_already_found and self.ask_the_user(semagram_to_add_name, slot_name, value.text[:value.text.find("#")] + " (from " + semagram_name + ")"):
                                wordnet_id = value.attrib["wnSynset"]
                                babelnet_id = value.attrib["babelsynset"]
                                pos_tag = value.text[value.text.find("#"):]
                                if wordnet_id != "":
                                    self.print_definition_of_wordnet_id(wordnet_id, value.text[:value.text.find("#")])
                                elif wordnet_id == "" and babelnet_id != "":
                                    print("\nAdded value " + value.text[:value.text.find("#")] + " with POS tag and BabelNet ID inherited\n")
                                elif wordnet_id == "" and babelnet_id == "":
                                    print("\nAdded value " + value.text[:value.text.find("#")] + " with POS tag but without IDs inherited\n")
                                semagrams_after_third_part.write("\n\t\t\t<value auto=\"yes\" babelsynset=\"" + babelnet_id + "\" wnSynset=\"" + wordnet_id + "\">" +
                                    value.text[:value.text.find("#")] + pos_tag + "</value>")
                                values_added += 1
        return values_added


    def add_semagrams(self, semagram_xml_name):
        """
        Write the semagrams file of the third part adding new semagrams with new slots and new values
        :param semagram_xml_name: semagram name
        :return: None
        """
        global total_semagrams_added, total_values_added, pos_tags_already_found, babelnet_ids_already_found, wordnet_ids_already_found, semagrams_already_found, semagrams_third_part_ids
        semagrams_added = 0
        semagrams_after_third_part = open("./semagrams_xml/semagrams_after_third_part_" + str(start) + "-" + str(end) + ".xml", "a")
        thesaurus = xml_parser.parse("./semagram_similar_xml/" + semagram_xml_name + "_similar.xml", xml_parser.XMLParser(remove_comments=True)).getroot()
        for keyword in thesaurus:
            for item in keyword:
                if item.text not in semagrams_already_found:
                    semagrams_already_found.append(item.text)
                    pos_tags_already_found = {}
                    babelnet_ids_already_found = {}
                    wordnet_ids_already_found = {}
                    semagram_to_add_name = item.text
                    semagrams_added += 1
                    semagrams_after_third_part.write("\n\t<semagram name=\"" + item.text + "\" synset=\"" + self.get_wordnet_id(semagrams_third_part_ids.get(item.text)) + "\" babelsynset=\"" + semagrams_third_part_ids.get(item.text) + "\">")
                    wsdiff = xml_parser.parse("./semagram_differences_xml/" + semagram_xml_name + "_vs_" + item.text + ".xml").getroot()
                    for relation in wsdiff:
                        gramrel_indexes = self.get_indexes_gramrel(relation.attrib["name"].replace("\"%w\"", "X"))
                        for gramrel_index in gramrel_indexes:
                            semagrams_after_third_part.write("\n\t\t<slot name=\"" + slots[gramrel_index] + "\">")
                            values_added = 0
                            values_already_found = []
                            for item in relation:
                                values_already_found.append(item.text)
                                if int(item.attrib["class"]) <= 0 and not self.remove_unsatisfactory_value_with_gensim(item.text, slots[gramrel_index]) and \
                                        self.ask_the_user(semagram_to_add_name, slots[gramrel_index], item.text):
                                    pos_tag, babelnet_id, wordnet_id = self.add_categorize_babelnet_id_and_wordnet_id_to_value(item.text, semagram_to_add_name)
                                    if wordnet_id != "":
                                        self.print_definition_of_wordnet_id(wordnet_id, item.text)
                                    elif wordnet_id == "" and pos_tag != "":
                                        print("\nAdded value " + item.text + " but not found in BabelNet and WordNet\n")
                                    elif wordnet_id == "" and pos_tag == "":
                                        print("\nValue " + item.text + " not found in the corpus and/or in resources\n")
                                    semagrams_after_third_part.write("\n\t\t\t<value auto=\"yes\" babelsynset=\"" + babelnet_id + "\" wnSynset=\"" + wordnet_id + "\">" +
                                                                     item.text + pos_tag + "</value>")
                                    values_added += 1
                            values_added = self.ask_values_of_similiar_semagram_to_user(semagram_xml_name, semagram_to_add_name, slots[gramrel_index], values_added, values_already_found, semagrams_after_third_part)
                            total_values_added += values_added
                            self.write_statistics("Values added in slot " + slots[gramrel_index] + " of semagram " + semagram_to_add_name + " --> " + str(values_added) + "\n")
                            semagrams_after_third_part.write("\n\t\t</slot>")
                    self.get_values_to_ask_of_original_semagram(semagram_xml_name, semagram_to_add_name, semagrams_after_third_part)
                    self.write_statistics("\n")
                    semagrams_after_third_part.write("\n\t</semagram>")
        total_semagrams_added += semagrams_added
        self.write_statistics("Semagrams added from semagram " + semagram_xml_name + " --> " + str(semagrams_added) + "\n\n" +
                              "--------------------------------------------------------------------------------\n\n")
        semagrams_after_third_part.close()


    def write_statistics(self, statistic_text):
        """
        Write statistics on the statistic file
        :param statistic_text: the text to write on file
        :return: None
        """
        statistics_file_third_part = open("./statistics_file/statistics_file_third_part_" + str(start) + "-" + str(end) + ".txt", "a")
        statistics_file_third_part.write(statistic_text)
        statistics_file_third_part.close()


    def load_gensim_model(self):
        """
        Load in RAM the Gensim model file
        :return: None
        """
        global model
        print("Loading Gensim model ...\n")
        model = gensim.models.KeyedVectors.load_word2vec_format('./gensim_model/GoogleNews-vectors-negative300.bin', binary=True)


    def semagrams_processing(self):
        """
        Processing semagrams
        :return: None
        """
        global start, end
        semagram_xml_names = sorted(os.listdir("./semagram_xml"))
        start = 1
        end = 0
        while start > end or start < 1 or start > 50 or end < 1 or end > 50:
            start = int(input("Insert start (1 - 50): "))
            end = int(input("Insert end (1 - 50): "))
            if start > end:
                print("\nStart must be lower or equal than end!")
            if start < 1 or start > 50:
                print("\nStart must be between 1 and 50")
            if end < 1 or end > 50:
                print("\nEnd must be between 1 and 50")
            print("\n", end="")
        current = 1
        self.reset_statistics_file()
        self.init_semagrams_after_third_part()
        for semagram_xml_name in semagram_xml_names:
            if not semagram_xml_name.startswith("."):
                if current >= start:
                    if current <= end:
                        self.get_slot_and_gramrel(semagram_xml_name.replace(".xml", ""))
                        self.add_semagrams(semagram_xml_name.replace(".xml", ""))
                        self.reset_slots_and_gramrels()
                    else:
                        break
                current += 1


    def get_semantic_propagation(self):
        """
        Get the semantic propagation
        :return: None
        """
        global start, end, total_semantic_propagation
        current = 1
        semagrams_similar = sorted(os.listdir("./semagram_similar_xml"))
        for semagram_similar in semagrams_similar:
            if not semagram_similar.startswith("."):
                if current >= start:
                    if current <= end:
                        semagrams_after_first_and_second_part = xml_parser.parse("./semagrams_xml/semagrams_after_first_and_second_part.xml").getroot()
                        for semagram_first_and_second_part in semagrams_after_first_and_second_part:
                            if semagram_first_and_second_part.attrib["name"] == semagram_similar.replace("_similar.xml", ""):
                                values = 0
                                cont = 0
                                semagram_similar_list = xml_parser.parse("./semagram_similar_xml/" + semagram_similar).getroot()
                                for slot_first_and_second_part in semagram_first_and_second_part:
                                    for value_first_and_second_part in slot_first_and_second_part:
                                        values += 1
                                        found = False
                                        for keyword in semagram_similar_list:
                                            for item in keyword:
                                                if found is False:
                                                    semagrams_after_third_part = xml_parser.parse("./semagrams_xml/semagrams_after_third_part_for_semantic_propagation.xml").getroot()
                                                    for semagram_third_part in semagrams_after_third_part:
                                                        if semagram_third_part.attrib["name"] == item.text:
                                                            for slot_third_part in semagram_third_part:
                                                                if slot_first_and_second_part.attrib["name"] == slot_third_part.attrib["name"]:
                                                                    for value_third_part in slot_third_part:
                                                                        if value_first_and_second_part.text[:value_first_and_second_part.text.find("#")] == value_third_part.text[:value_third_part.text.find("#")] and \
                                                                                value_first_and_second_part.attrib["babelsynset"] == value_third_part.attrib["babelsynset"] and \
                                                                                value_first_and_second_part.attrib["wnSynset"] == value_third_part.attrib["wnSynset"]:
                                                                            found = True
                                                                            cont += 1
                                self.write_statistics("Semantic propagation of semagram " + semagram_similar.replace("_similar.xml", "") + " --> " + str('%.3f' % float(cont / values)) +
                                                      " (" + str(cont) + "/" + str(values) + ")\n")
                                total_semantic_propagation += (cont / values)
                    else:
                        break
                current += 1
        self.write_statistics("\n--------------------------------------------------------------------------------\n\n")
        os.remove("./semagrams_xml/semagrams_after_third_part_for_semantic_propagation.xml")



if __name__ == "__main__":
    warnings.simplefilter(action='ignore', category=FutureWarning)
    class_instance = third_part()
    time_start = time.time()
    class_instance.load_gensim_model()
    # class_instance.filter_wikipedia_sentences()     Eseguito solo la prima volta per ottenere il file permanente
    class_instance.init_semagrams_already_found()
    class_instance.get_semagrams_third_part_ids()
    class_instance.semagrams_processing()
    class_instance.save_semagrams_after_third_part_for_semantic_propagation()
    class_instance.finish_semagrams_after_third_part()
    class_instance.get_semantic_propagation()
    class_instance.write_statistics("Total semagrams added --> " + str(total_semagrams_added))
    class_instance.write_statistics("\nTotal values added --> " + str(total_values_added))
    class_instance.write_statistics("\nAverage number of values added for each semagram --> " + str('%.3f' % float(total_values_added / total_semagrams_added)))
    class_instance.write_statistics("\nTotal semantic propagation --> " + str('%.3f' % float(total_semantic_propagation)))
    class_instance.write_statistics("\nTime of execution --> " + str(int(time.time() - time_start)) + " seconds")